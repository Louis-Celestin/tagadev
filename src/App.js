import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./components/home/Home";
import Header from "./components/Header";
import TypeSignaletique from "./components/home/TypeSignaletique";
import DemandeProcess from "./components/home/DemandeProcess";
import ContactFooter from "./components/home/ContactFooter";
import Inscription from "./components/components1/Inscription";

import { Routes, Route } from "react-router-dom";
import Connexion from "./components/components1/Connexion";

function App() {
  return (
    <div className="App">
      <Header />
      <Home />
      <TypeSignaletique />
      <DemandeProcess />


      <ContactFooter />
      {/* <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/signaletique" element={<TypeSignaletique />} />

        <Route path="/header" element={<Header />}>
          
          <Route path="/header/signin" element={<Inscription />} />
        </Route>
      </Routes> */}
    </div>
  );
}

export default App;
