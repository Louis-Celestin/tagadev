import React from "react";
import style from "./ItemCart.module.css";
import DriveFileRenameOutlineIcon from "@mui/icons-material/DriveFileRenameOutline";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
export default function ItemCart() {
  return (
    <div className={style.ItemCart}>
      <DeleteForeverIcon className={style.ItemCart_btnDelete}/>
      <div className={style.ItemCart_Image}></div>
      <div className={style.ItemCart_Info}>
        <p className={style.ItemCart_Info_title}>
          Signaletique rectangle directionnel
        </p>
        <p className={style.ItemCart_Info_price}>3500 F</p>
        <p className={style.ItemCart_Info_btn}>
          <DriveFileRenameOutlineIcon /> <p style={{marginLeft:5}}>Modifier</p>
        </p>
      </div>
    </div>
  );
}
