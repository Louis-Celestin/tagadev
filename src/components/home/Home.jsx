import React from 'react';

import '../../styles/homeStyle/home.css';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom';

const Home = () => {
  const data=[
    {
      id:1,
      titre:"Bienvenue sur taga.ci.",
      description:"Réservez votre signalétique pour une direction claire",
      image:require("../../assets/cadre.png"),
      

    },
    {
      id:2,
      titre:"Personnalisez votre signalétique:",
      image:require("../../assets/cadre.png"),
      description:"Créer des signalétiques à votre goût et à moindre coût.",
    }
  ]

  return (

    <div>
      <Carousel>
      
      {data.map((slide)=>(
        <div key={slide.id} className='div-slider'>
          <div className='overlay'>
            <span className='overlay_title' >{slide.titre}</span>
            <p className='overlay_description'>{slide.description} </p>
            <button className='btn-commande'> <Link to='/commande'>Commander</Link> </button>

          </div>

        </div>
      ) )}
      
      </Carousel>

    </div>
    
  )
}

export default Home