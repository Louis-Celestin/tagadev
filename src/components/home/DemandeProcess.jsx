import React from 'react';
import '../../styles/homeStyle/demandeProcess.css';
import pngwing5 from '../../assets/pngwing5.png';


const DemandeProcess = () => {
  return (
    <div className='container-process'>
        <div className='step-container'>
            <h1 >Processus de demande d'une signalétique</h1>
            <h4 className='question-title'>Comment commander sa signalétique?</h4>
            <ul className='list-step'>
                <li><strong>Etape 1 </strong>:  Se rendre sur la plateforme et choisir un type de signalétique.</li>
                <li><strong>Etape 2</strong>  : Personnaliser la signalétique avec un texte de vos choix</li>
                <li><strong> Etape 3</strong> : Passer à la commande et aux paiement de la signalétique</li>
                <li><strong>Etape 4 </strong> : Vous serez informé une fois votre signalétique prête</li>
                <li><strong>Etape 5 </strong> : Installation de votre signalétique dans 24h</li>
            </ul>

        </div>

        <div className='container-img-signaletique'>
            <img src={pngwing5} alt=''/>
        </div>
        

    </div>
  )
}

export default DemandeProcess