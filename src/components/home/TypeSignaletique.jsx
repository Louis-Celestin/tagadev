import React from "react";
import { listSignaletique } from "../datas/Data.js";
import "../../styles/homeStyle/typeSignaletique.css";
import { Link } from "react-router-dom";

const TypeSignaletique = () => {

  return (
    <div className="container-type-signaletique">
      <span className="title-desc-signaletique">Nos signalétiques</span>
      <p className="description-signaletique">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus
        doloremque ut accusantium voluptates fugit, sequi numquam magni porro,
        voluptas ea perspiciatis quos dicta velit nisi quia sit aliquam adipisci
        optio.
      </p>

      <div className="list-signaletique">
        <a href="/signaletique" className="type-signaletique">
          Signalétique routière
        </a>
        <a href="/signaletique" className="type-signaletique">
          Signalétique publicitaire
        </a>
        <a href="/signaletique" className="type-signaletique">
          Signalétique directionnelle
        </a>
      </div>
      <div className="row">
        {listSignaletique.map((liste) => (
          <div class="col-md-6 col-xs-4 col-lg-4">
            <div key={liste.id} className="list-type-signaletique" class="card">
              <div className="image-signaletique">
                <img src={liste.image} alt="image" />
              </div>

              <div class="card-body">
                <span className="title-signaletique">{liste.title}</span>
                <span className="price">{liste.price}</span>
                <button className="btn-panier">
                  Ajouter au panier
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TypeSignaletique;
