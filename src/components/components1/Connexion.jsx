
import {React, useState} from 'react'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import '../../styles/style1/connexion-page.css'




function Connexion() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <button className='btn-connexion' onClick={handleShow}>
        Connectez-vous
      </button>

      <Modal show={show} onHide={handleClose}>
       
        <Modal.Body>
          
        <p className="mt-5 text-muted">Connectez-vous à votre compte</p>
        <Form id="sign-in-form" className="text-center p-3 w-100">

      <Form.Group controlId="sign-in-email-address">
        <Form.Control type="email" size="lg" placeholder="Email address" autoComplete="username" className="position-relative" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="sign-in-password">
        <Form.Control type="password" size="lg" placeholder="Password" autoComplete="current-password" className="position-relative" />
      </Form.Group>

      <div className="d-grid">
        <Button variant="primary" size="lg">Sign in</Button>
      </div>
      <p className="mt-5 text-muted">&copy; 2021-2022</p>
     
    </Form>

        </Modal.Body>
      </Modal>
    </>
  );
}
export default Connexion