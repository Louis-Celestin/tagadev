import React, { useState } from "react";
import "../styles/header.css";
import { Link } from "react-router-dom";
import Divider from "@material-ui/core/Divider";
import logo from "../assets/logo.png";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../styles/style1/connexion-page.css";
import DrawerCart from "./drawer/DrawerCart";

const Header = () => {
  const [show, setShow] = useState(false);
  const [openDrawer, setOpenDrawer] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [inputs, setInputs] = useState({});

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(inputs);
  };
  const openDrawerCart = ()=>{
    setOpenDrawer(true)
  }
  const closeDrawerCart = ()=>{
    setOpenDrawer(false)
  }
  return (
    <div className="nav-component">
      {/* <Modal show={show} onHide={handleClose}>
        
        <Modal.Body><i class="fa-regular fa-circle-check"></i></Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Body>
          <p className="mt-5 text-muted">Connectez-vous à votre compte</p>
          <Form
            id="sign-in-form"
            className="text-center p-3 w-100"
            onSubmit={handleSubmit}
          >
            <Row className="mb-3">
              <Form.Group as={Col} md="12">
                <Form.Control
                  type="text"
                  placeholder="Email"
                  required
                  name="email"
                  onChange={handleChange}
                  className="input-email"
                  value={inputs.email}
                />
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group as={Col} md="12">
                <Form.Control
                  type="password"
                  placeholder="Password"
                  required
                  value={inputs.password}
                  name="password"
                  onChange={handleChange}
                  className="input-password"
                />
              </Form.Group>
            </Row>

            <div className="div-copyright">
              <button className="btn btn-connexion">Sign in</button>
            </div>
            <p className="mt-5 text-muted">&copy; 2021-2022</p>
          </Form>
        </Modal.Body>
      </Modal>

      <div className="nav-head">
        <div>
          <img src={logo} alt="logo" />
        </div>
        <div className="search">
          <i class="fa-solid fa-magnifying-glass"></i>
          <input type="search" placeholder="RECHERCHER" />
        </div>
        <div className="person-icon">
          <div onClick={openDrawerCart} >
            <i class="fa fa-cart-shopping"></i>
          </div>
          <div class="dropdown">
            <button id="myBtn" class="dropbtn">
              <i class="fa-solid fa-user-plus"></i>
            </button>
            <DrawerCart open={openDrawer} close={closeDrawerCart}/>
            <div id="myDropdown" class="dropdown-content">
              <div class="btn btn-dropdown">
                <button>
                  <Link to="/signin">Creer un compte</Link>
                </button>
              </div>
              <div class="btn btn-dropdown">
                <button onClick={handleShow}>Connexion</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <nav>
        <Link to="#" className="item">
          Acceuil
        </Link>
        <Link to="#signaletique" className="item">
          Les signalétiques
        </Link>
        <Link to="#" className="item">
          Mes commandes
        </Link>
        <Link to="#" className="item">
          Contactez-nous
        </Link>
      </nav>
      <Divider className="divider" />
    </div>
  );
};

export default Header;
