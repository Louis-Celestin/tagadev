import { Drawer } from "@mui/material";
import React from "react";
import style from "./Drawer.module.css";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ItemCart from "../ItemCart/ItemCart";
import ButtonCustom from "../buttons/ButtonCustom";
export default function DrawerCart(props) {
  return (
    <Drawer
      anchor="right"
      open={true}
      // open={props.open}
      onClose={props.close}
    >
      <div className={style.DrawerCart}>
        <div className={style.DrawerCart_Header}>
          <span onClick={props.close}>
            <ArrowBackIcon style={{ cursor: "pointer" }} />
          </span>
          <div style={{ marginLeft: 20, color: "white" }}>Mon panier</div>
        </div>
        <div className={style.DrawerCart_Body}>
          <ItemCart />
        </div>
        <div className={style.DrawerCart_Footer}>
          <ButtonCustom title={"VOIR MON PANIER"} />
        </div>
      </div>
    </Drawer>
  );
}
